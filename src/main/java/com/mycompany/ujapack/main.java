package com.mycompany.ujapack;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 *
 * @author inmar
 */
@EnableAutoConfiguration
public class main {
    public static void leerJ(String file, Map<Integer,CentroLogistico> centros, Map<Integer,Oficina> oficinas) throws IOException {
        FileReader fr = new FileReader(file);

        BufferedReader br = new BufferedReader(fr);
        StringBuilder strB= new StringBuilder();
        String srtAux = null;
        while((srtAux=br.readLine())!=null){
            strB.append(srtAux);
        }
        String jsonStr = strB.toString();
        JsonObject raiz = new Gson().fromJson(jsonStr, JsonObject.class);
        int idOficina =1;
        Set<String> centrosLogSetStr = raiz.keySet();
        for(String centroStr : centrosLogSetStr){

            JsonObject centroJson= raiz.getAsJsonObject(centroStr);

            int id = Integer.parseInt(centroStr);
            String nombre = centroJson.get("nombre").getAsString();
            String localizacion = centroJson.get("localización").getAsString();
            CentroLogistico c= new CentroLogistico(id,nombre,localizacion);


            JsonArray prov= centroJson.getAsJsonArray("provincias");


            for(JsonElement provincia : prov){
                String nom = provincia.getAsString();
                Oficina o = new Oficina(idOficina, provincia.getAsString(), id);

              c.oficinas.put(idOficina,o);

              //rellenar mapa de oficinas
              oficinas.put(idOficina,o);
              idOficina++;
            }


            JsonArray conexiones = centroJson.getAsJsonArray("conexiones");
            ArrayList<Integer> conexTemp = new ArrayList<>();

            for(JsonElement conexion : conexiones){
               c.conexiones.add(Integer.parseInt(conexion.getAsString()));
            }

            centros.put(id,c);


        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {


    }
}
