package com.mycompany.ujapack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
class UjaPackImpl {
    public static void main(String[] args) throws Exception {

        SpringApplication servidor = new SpringApplication(UjaPackImpl.class);
        ApplicationContext context = servidor.run(args);


    }
}

