/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ujapack;

/**
 *
 * @author inmar
 */
public class Oficina{

    private int id;
    private String nombre;
    private int idCLPertenece;

    public Oficina(int id, String nombre, int idCLPertenece) {
        this.id = id;
        this.nombre = nombre;
        this.idCLPertenece = idCLPertenece;
    }




    public int getIdCLPertenece() {
        return idCLPertenece;
    }



    @Override
    public String toString() {
        return "Oficina{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +

                '}';
    }
}
