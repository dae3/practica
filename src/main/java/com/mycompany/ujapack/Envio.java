/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ujapack;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author inmar
 */
public class Envio {
    
    public enum Estado{
        enTransito,
        enReparto,
        entregado


    }

    private int idOficinaDestino;
    private int idOficinaOrigen;
     private int localizador;
    private String destinatario;
    private String remitente;
    private float peso;
    private float alto;
    private float ancho;
    private float largo;
    private double dimensiones;
    private Estado estado;
    private LocalDateTime fechaEntrega;
    private double importe;
    public ArrayList<PuntoDeControl> ruta;

    
    //Constructor

    public Envio(int localizador,String destinatario, String remitente, LocalDateTime fechaEntrega,
                 int idOficinaDestino, int idOficinaOrigen, ArrayList<PuntoDeControl> ruta,float peso,
                 double dimensiones) {
        this.localizador=localizador;
        this.destinatario = destinatario;
        this.remitente = remitente;
        this.estado = estado.enTransito;
        this.fechaEntrega = fechaEntrega;
        this.idOficinaDestino = idOficinaDestino;
        this.idOficinaOrigen = idOficinaOrigen;
        this.ruta= ruta;
        this.peso= peso;
        this.dimensiones=dimensiones;
        this.importe = calcularImporte(this.peso, this.dimensiones, this.ruta.size());


    }

    public int getLocalizador() {
        return localizador;
    }

    public void setLocalizador(int localizador) {
        this.localizador = localizador;
    }


    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public LocalDateTime getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDateTime fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public List<PuntoDeControl> getRuta() {
        return ruta;
    }

    public void setRuta(ArrayList<PuntoDeControl> ruta) {
        this.ruta = ruta;
    }

    public double getImporte() {
        return importe;
    }

    private  double calcularImporte(float peso, double dimensiones, int tamRuta){
        importe = peso * dimensiones * (tamRuta + 1) / 1000;
        return importe;
    }


    @Override
    public String toString() {
        return "Envio{" +
                "idOficinaDestino=" + idOficinaDestino +
                ", idOficinaOrigen=" + idOficinaOrigen +
                ", localizador=" + localizador +
                ", destinatario='" + destinatario +
                ", remitente='" + remitente +
                ", peso=" + peso +
                ", dimensiones=" + dimensiones +
                ", estado=" + estado +
                ", fechaEntrega=" + fechaEntrega +
                ", importe=" + importe +"\n"+
                ", ruta=" + ruta +
                '}';
    }
}
