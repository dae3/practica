/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ujapack;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author inmar
 */
public class CentroLogistico{
    
    private int id;
    private String nombre;
    private String localizacion;
    public ArrayList<Integer> conexiones = new ArrayList<>();
    public Map<Integer,Oficina> oficinas = new HashMap<>();

    public CentroLogistico(int id, String nombre, String localizacion) {
        this.id = id;
        this.nombre = nombre;
        this.localizacion = localizacion;

    }

    @Override
    public String toString() {
        return "CentroLogistico{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", localizacion='" + localizacion + '\'' +
                ", conexiones=" + conexiones +
                ", oficinas=" + oficinas +
                '}';
    }
}
