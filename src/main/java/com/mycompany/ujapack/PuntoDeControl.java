package com.mycompany.ujapack;

import java.time.LocalDateTime;

/**
 *
 * @author inmar
 */
public class PuntoDeControl {

    private LocalDateTime fechaSalida;
    private  LocalDateTime fechaEntrada;
    private int idPertenece;
    private boolean isCentrologistico;
    private boolean activo;

    public PuntoDeControl(LocalDateTime fechaSalida, LocalDateTime fechaEntrada, int idPertenece,
                          boolean isCentrologistico, boolean activo) {

        this.fechaSalida = fechaSalida;
        this.fechaEntrada = fechaEntrada;
        this.idPertenece = idPertenece;
        this.isCentrologistico = isCentrologistico;
        this.activo = activo;
    }

    public LocalDateTime getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(LocalDateTime fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public LocalDateTime getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(LocalDateTime fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public boolean isActivo() {
        return activo;
    }

    public int getIdPertenece() {
        return idPertenece;
    }

    public boolean isCentrologistico() {
        return isCentrologistico;
    }

    @Override
    public String toString() {
        if (isCentrologistico == false) {

            return "PuntoDeControl{" +
                    "fechaSalida=" + fechaSalida +
                    ", fechaEntrada=" + fechaEntrada +
                    ", id Oficina=" + idPertenece +
                    ", activo=" + activo +
                    '}';

        } else {
            return "PuntoDeControl{" +
                    "fechaSalida=" + fechaSalida +
                    ", fechaEntrada=" + fechaEntrada +
                    ", id Centro logistico=" + idPertenece +
                    ", activo=" + activo +
                    '}';
        }

    }
}
