package Beans;


import com.mycompany.ujapack.CentroLogistico;
import com.mycompany.ujapack.Envio;
import com.mycompany.ujapack.Oficina;
import com.mycompany.ujapack.PuntoDeControl;


import java.time.LocalDateTime;
import java.util.*;

/**
 * @author inmar
 */

public class UjaPack {

    private final Map<Integer, CentroLogistico> centrosLogisticos;
    private final Map<Integer, Oficina> oficinas;
    private final Map<Integer, Envio> envios = new HashMap<>();
    private int localizador = 0;

    public UjaPack(Map<Integer, CentroLogistico> centrosLogisticos, Map<Integer, Oficina> oficinas) {
        this.centrosLogisticos = centrosLogisticos;
        this.oficinas = oficinas;
    }

//devuelve el punto de control activo si encuentra el envio en el mapa de envios
    public PuntoDeControl consultaPTEnvio(int localizadorEnvio) {
        Envio envio;
        if (envios.containsKey(localizadorEnvio)) {
            envio = envios.get(localizadorEnvio);

            for (int i = 0; i <= envio.ruta.size(); i++) {
                if (envio.ruta.get(i).isActivo())
                    return envio.getRuta().get(i);

            }
        }
        return null;
    }
// devuelve la ruta que deberia de seguir el envio
    public ArrayList<PuntoDeControl> consultaRutaEnvio(int localizadorEnvio) {
        Envio envio;
        if (envios.containsKey(localizadorEnvio)) {
            envio = envios.get(localizadorEnvio);
            return envio.ruta;
        }
        return null;
    }
//crea un envio y llama al metodo para crear la ruta
    public Envio crearEnvio(String remitente, String destinatario, float peso,
                            float alto, float ancho, float largo,
                            int idOficinaDestino, int idOficinaOrigen) {
        double dimensiones = alto * ancho * largo;

        ArrayList<PuntoDeControl> ruta = calcularRuta(idOficinaOrigen, idOficinaDestino);
        Envio envio = new Envio(this.localizador, destinatario, remitente, null, idOficinaDestino, idOficinaOrigen, ruta, peso, dimensiones);
        envios.put(envio.getLocalizador(), envio);
        this.localizador++;


        return envio;
    }
    // calcula y devuelve la ruta
    private ArrayList<PuntoDeControl> calcularRuta(int idOficinaOrigen, int idOficinaDestino) {

        ArrayList<PuntoDeControl> ruta = new ArrayList<>();

        Oficina oficinaOrigen = oficinas.get(idOficinaOrigen);
        Oficina oficinaDestino = oficinas.get(idOficinaDestino);
        int idCentroLOficinaOrigen, idCentroLOficinaDestino;

        idCentroLOficinaOrigen = oficinaOrigen.getIdCLPertenece();
        idCentroLOficinaDestino = oficinaDestino.getIdCLPertenece();

        Queue<Integer> rutamenor = new LinkedList<>();

        boolean[] marcados = new boolean[11];
        for (int i = 0; i < 11; i++) {
            marcados[i] = false;
        }

        if (idOficinaDestino == idOficinaOrigen) {
            PuntoDeControl p = new PuntoDeControl(null, LocalDateTime.now(), idOficinaOrigen
                    , false, true);
            ruta.add(p);


        } else if (idCentroLOficinaOrigen == idCentroLOficinaDestino && idOficinaDestino != idOficinaOrigen) {
            PuntoDeControl p1 = new PuntoDeControl(null, LocalDateTime.now(), idOficinaOrigen
                    , false, true);
            PuntoDeControl p2 = new PuntoDeControl(null, null, idCentroLOficinaOrigen
                    , true, false);
            PuntoDeControl p3 = new PuntoDeControl(null, null, idOficinaDestino
                    , false, false);
            ruta.add(p1);
            ruta.add(p2);
            ruta.add(p3);

        } else {
            PuntoDeControl p = new PuntoDeControl(null, LocalDateTime.now(), idOficinaOrigen
                    , false, true);
            ruta.add(p);
            PuntoDeControl p2 = new PuntoDeControl(null, null, idCentroLOficinaOrigen
                    , true, false);
            ruta.add(p2);


            camino(idCentroLOficinaOrigen, idCentroLOficinaDestino, ruta, rutamenor, marcados);
            for (int i = 0; i <= rutamenor.size(); i++) {
                int j = rutamenor.poll();

                PuntoDeControl p5 = new PuntoDeControl(null, null, j, true, false);
                ruta.add(p5);

            }
            PuntoDeControl p3 = new PuntoDeControl(null, null, idOficinaDestino
                    , false, false);
            ruta.add(p3);
        }


        return ruta;
    }
    //calcula la ruta si el centro logistico de origen es distinto del de destino
    private void camino(int idCentroLOficinaOrigen, int idCentroLOficinaDestino,
                        ArrayList<PuntoDeControl> ruta, Queue rutamenor, boolean[] marcados) {
        CentroLogistico c1;
        c1 = centrosLogisticos.get(idCentroLOficinaOrigen);


        marcados[idCentroLOficinaOrigen] = true;
        rutamenor.add(idCentroLOficinaOrigen);
        for (int i = 0; i < c1.conexiones.size(); i++) {
            if (!marcados[c1.conexiones.get(i)]) {
                if (c1.conexiones.get(i) == idCentroLOficinaDestino) {
                    rutamenor.add(c1.conexiones.get(i));

                } else {
                    marcados[c1.conexiones.get(i)] = true;
                    camino(c1.conexiones.get(i), idCentroLOficinaDestino, ruta, rutamenor, marcados);
                }
            }


        }
        rutamenor.remove();

    }

    //avanza un paquete si lo encuentra en el mapa de de envios
    public void avanzaPaquete(int localizadorEnvio) {

        Envio envio;
        if (envios.containsKey(localizadorEnvio)) {
            envio = envios.get(localizadorEnvio);
            //posicion del punto de control dnde estamos
            int auxiliar = 0;
            int tam = envio.getRuta().size() - 1;
            for (int i = 0; i < envio.getRuta().size(); i++) {
                if (envio.getRuta().get(i).isActivo() == true) {
                    auxiliar = i;

                }
            }

            if (auxiliar == tam && envio.getEstado() != Envio.Estado.enReparto) {
                envio.ruta.get(auxiliar).setFechaSalida(LocalDateTime.now());
                envio.ruta.get(auxiliar).setActivo(false);
                envio.setEstado(Envio.Estado.enReparto);

            } else if (envio.getEstado() == Envio.Estado.enReparto) {
                envio.setEstado(Envio.Estado.entregado);
                envio.setFechaEntrega(LocalDateTime.now());

            } else {
                envio.ruta.get(auxiliar).setFechaSalida(LocalDateTime.now());
                envio.ruta.get(auxiliar + 1).setFechaEntrada(LocalDateTime.now());
                envio.ruta.get(auxiliar).setActivo(false);
                envio.ruta.get(auxiliar + 1).setActivo(true);
            }

        }
    }

    // avanza todos los paquetes
    public void avanzaPaquetes() {
        Envio envio;

        for (Map.Entry<Integer, Envio> entry : envios.entrySet()) {

            envio = entry.getValue();
            int auxiliar = 0;
            int tam = envio.getRuta().size() - 1;
            for (int i = 0; i < envio.ruta.size(); i++) {
                if (envio.ruta.get(i).isActivo() == true) {
                    auxiliar = i;

                }
            }

            if (auxiliar == tam && envio.getEstado() != Envio.Estado.enReparto) {
                envio.ruta.get(auxiliar).setFechaSalida(LocalDateTime.now());
                envio.ruta.get(auxiliar).setActivo(false);
                envio.setEstado(Envio.Estado.enReparto);

            } else if (envio.getEstado() == Envio.Estado.enReparto) {
                envio.setEstado(Envio.Estado.entregado);
                envio.setFechaEntrega(LocalDateTime.now());

            } else {
                envio.ruta.get(auxiliar).setFechaSalida(LocalDateTime.now());
                envio.ruta.get(auxiliar + 1).setFechaEntrada(LocalDateTime.now());
                envio.ruta.get(auxiliar).setActivo(false);
                envio.ruta.get(auxiliar + 1).setActivo(true);
            }

        }
    }

}
