package com.mycompany.ujapack;


import Beans.UjaPack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {UjaPack.class})
class UjaPackTest {
    @Autowired
    UjaPack u;

    //comprueba el caso de que la oficina de origen y destino sea la misma
    //y el importe lo devuelve correctamente
    @Test
    public void creaEnvioYrutaCaso1() {
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();

        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 1, 1);
        Assertions.assertEquals(1, e1.getRuta().size());
        Assertions.assertEquals(0.002, e1.getImporte());


    }

    //comprueba el caso de que la oficina de origen y destino sean distintas pero pertenezcan al mismo centro logistico
    //y el importe lo devuelve correctamente
    @Test
    public void creaEnvioYrutaCaso2() {
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 1, 3);
        Assertions.assertEquals(3, e1.getRuta().size());
        Assertions.assertEquals(0.004, e1.getImporte());


    }
    //comprueba el caso de que la oficina de origen y destino sean distintas y pertenezcan a distinto centro logistico
    //y el importe lo devuelve correctamente

    @Test
    public void creaEnvioYrutaCaso3() {
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 34, 1);
        Assertions.assertEquals(5, e1.getRuta().size());
        Assertions.assertEquals(0.006, e1.getImporte());


    }
    //comprueba que se lea bien el json, comprobando que existen los 10 centros logisticos y 52 oficinas
    @Test
    public void leerjson() {
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Assertions.assertEquals(10, centros.size());
        Assertions.assertEquals(52, oficinas.size());


    }

    //comprueba que los puntos de control estan activos cuando le corresponte
    // y que el paquete se entrega correctamente siguiendo la ruta
    @Test
    public void puntoControlActivo() {
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 1, 3);
        assertTrue(e1.getRuta().get(0).isActivo());
        assertFalse(e1.getRuta().get(1).isActivo());
        for (boolean b : new boolean[]{true, false}) {
            assertFalse(e1.getRuta().get(2).isActivo());
            u.avanzaPaquete(e1.getLocalizador());
            assertFalse(e1.getRuta().get(0).isActivo());
            assertEquals(b, e1.getRuta().get(1).isActivo());
        }
        assertTrue(e1.getRuta().get(2).isActivo());
        u.avanzaPaquete(e1.getLocalizador());
        assertFalse(e1.getRuta().get(0).isActivo());
        assertFalse(e1.getRuta().get(1).isActivo());
        assertFalse(e1.getRuta().get(2).isActivo());
        assertSame(e1.getEstado(), Envio.Estado.enReparto);
        u.avanzaPaquete(e1.getLocalizador());
        assertSame(e1.getEstado(), Envio.Estado.entregado);
    }

    //comprobar que se obtiene el punto de control activo correctamente
    @Test
    public void devolver1PT(){
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 1, 3);
        PuntoDeControl p = u.consultaPTEnvio(e1.getLocalizador());
        assertNotEquals(-1, p.getIdPertenece());
        assertTrue(p.isActivo());


    }

    //comprobar que devuelve la ruta de puntos de control entera y correctamente
    @Test
    public void devolverRuta() {
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 1, 3);
        ArrayList<PuntoDeControl> rt = u.consultaRutaEnvio(e1.getLocalizador());
        assertNotEquals(rt.size(), 0);
        assertTrue(rt.get(0).isActivo());
        assertEquals(rt.get(0).getIdPertenece(), 3);
        assertEquals(rt.get(1).getIdPertenece(), 1);
        assertTrue(rt.get(1).isCentrologistico());
    }

    @Test
    public void avanzarPaquetes(){
        Map<Integer, CentroLogistico> centros = new HashMap<>();
        Map<Integer, Oficina> oficinas = new HashMap<>();
        try {
            main.leerJ("redujapack.json", centros, oficinas);
        } catch (IOException e) {
            Assertions.assertFalse(true);

        }
        u = new UjaPack(centros, oficinas);
        Envio e1 = u.crearEnvio("a", "b", 1, 1, 1, 1, 1, 3);
        Envio e2 = u.crearEnvio("c", "d", 1, 1, 1, 1, 20, 1);
        for (Envio envio : Arrays.asList(e1, e2)) {
            assertTrue(envio.getRuta().get(0).isActivo());
        }
        u.avanzaPaquetes();
        for (Envio envio : Arrays.asList(e1, e2)) {
            assertTrue(envio.getRuta().get(1).isActivo());
        }

        for (Envio envio : Arrays.asList(e1, e2)) {
            assertFalse(envio.getRuta().get(0).isActivo());
        }


    }
}